# \swc\_d()
> Version: 0.1.0 (03/04/2020)
```php
namespace swc;
function _d(string $msg,$data=null,$di=null):?string{}
```
Записать сообщение отладки.

Сообщение заносится в $_GLOBALS['swc._dbg'][$di];

## Параметры
### $msg
Сообщение.
### $data
Дополнительные данные.

Рекомендуется использовать массив.
### $di
Идентификатор блока сообщений.

Если не задан - сгенерируется новый.

## Результат
'string|NULL'

идентификатор блока (группы) сообщений.

## Пример использования
```php

function foo(){
    $di=\swc\_d('Enter foo()');
    ...
    \swc\_d('Leave foo()');
}

$di=\swc\_d('Before call foo()');

foo();

\swc\_d('After call foo()');

```

