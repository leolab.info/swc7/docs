# \swc\Error()
> Version: 0.1.0 (05/04/2020)
```php
namespace swc;
function Error(string $err,array $data=[],bool $ret=false):false{}
```

Выполнить исключение.

## Параметры

### $err
`string`

Идентификатор исключения.

### $data
`array`

Данные для исключения.

### $ret
`bool`

При установке в `TRUE` из функции производится возврат.

В противном случае выбрасывается исключение заданное идентификатором.

## Результат
`bool` = `FALSE`

