# \swc\_autoload()
> Version: 0.1.0 (03/04/2020)
```php
namespace swc;
function _autoload(string $cName){}
```

SPL-загрузчик классов.

Порядок поиска файла класса:
- Класс без пространства имен:
	1. [\swc\base_path](./const.md#base_path)/[\swc\mdl_folder](./const.md#mdl_folder)/```class_name```/class.php
	1. [\swc\base_path](./const.md#base_path)/[\swc\mdl_folder](./const.md#mdl_folder)/```class_name```.class.php
	1. [\swc\swc_base](./const.md#swc_base)/mdl/```class_name```/class.php
	1. [\swc\swc_base](./const.md#swc_base)/mdl/```class_name```.class.php
	1. [\swc\swc_base](./const.md#swc_base)/```class_name```.class.php
- Класс в пространстве имен:
	1. [\swc\base_path](./const.md#base_path)/[\swc\mdl_folder](./const.md#mdl_folder)/```namespace```/```class_name```/class.php
	1. [\swc\base_path](./const.md#base_path)/[\swc\mdl_folder](./const.md#mdl_folder)/```namespace```/```class_name```.class.php
	1. [\swc\swc_base](./const.md#swc_base)/mdl/```namespace```/```class_name```/class.php
	1. [\swc\swc_base](./const.md#swc_base)/mdl/`namespace`/`class_name`.class.php

После загрузки класса производится вызов метода `$cName::__init()` при наличии.

## Изменения
### 2020-04-03
- перенос функции.
