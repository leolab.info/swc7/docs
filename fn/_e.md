# \swc\_e()
> Version: 0.1.0 (03/04/2020)
```php
namespace swc;
function _e(string $msg,$data=null,$di=null):?string{}
```

Записать сообщение об ошибке.

