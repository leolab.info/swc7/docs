# Константы ядра системы

[[_TOC_]]

Константы определяются в файле [swc_base](#swc_base)/inc.php

Все константы, кроме [swc_base](#swc_base) могут быть определены:
- до подключения файла [swc_base](#swc_base)/inc.php
- через переменные окружения

Для веб-сервера Apache переменные окружения определяются в файле /.htaccess (как правило):
```
SetEnv %var_name% %var_value%
```

- [swc_base](#swc_base)
- [base_path](#base_path)
- [data_path](#data_path)
- [mdl_folder](#mdl_folder)
- [dbg_enabled](#dbg_enabled)
- [dbg_file](#dbg_file)
- [err_file](#err_file)
- [log_file](#log_file)

### base_path
```php
const string base_path=$_SERVER['DOCUMENT_ROOT'];
```
Папка корня системы.

Переменная окружения: `swc_basePath`

По умолчанию = `$_SERVER['DOCUMENT_ROOT']`

### data_path
```php
const string data_path=base_path.'/data';
```
Папка для хранения данных системы.

Переменная окружения: `swc_dataPath`

По умолчанию = `base_path`/data

### swc_base
```php
const string swc_base=dirname(__FILE__);
```
Папка системы.  

Определяется автоматически.

### mdl_folder
```php
const string mdl_folder='mdl';
```
Имя папки размещения дополнительных модулей.


Используется:
- [_autoload()](./fn/_autoload.md)

Переменная окружения: `swc_mdlFolder`

По умолчанию = `mdl`

### dbg_enabled
```php
const bool dbg_enabled=false;
```

Включение режима отладки.

Переменная окружения: `swc_dbgEnabled`

По умолчанию = `false`

Используется:
- [_shutdown()](./fn/_shutdown.md)

### dbg_file
> Version: 0.1.0 (03/04/2020)
```php
const string dbg_file;
```
Файл для записи отладочной информации.

Переменная окружения: `swc_dbgFile`

### err_file
> Version: 0.1.0 (03/04/2020)
```php
const string err_file;
```

Файл для записи информации об ошибках

Переменная окружения: `swc_errFile`

Используется:
- [_shutdown()](./fn/_shutdown.md)

### log_file
> Version: 0.1.0 (03/04/2020)
```php
const string log_file;
```

Файл для записи информации о запросах и их обработке.

Переменная окружения: `swc_logFile`

Используется: [_shutdown()](./fn/_shutdown.md)
