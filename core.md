# Ядро

[[_TOC_]]

## Структура файлов
- [`swc_base`](./const.md#swc_base)
    - **mdl**/
        - **swc**
    - .htaccess
    - inc.php
    - index.php
    - swc.class.php

## Константы
- [base_path](./const.md#base_path)
- [swc_base](./const.md#swc_base)
- [mdl_folder](./const.md#mdl_name)
- [dbg_enabled](./const.md#dbg_enabled)
- [dbg_file](./const.md#dbg_file)
- [err_file](./const.md#err_file)
- [log_file](./const.md#log_file)

## Функции и классы
- [swc{}](./c/swc.md)
- [fs{}](./c/fs.md)
- [cf{}](./c/cf.md)
- [cfg{}](./c/cfg.md)
- [dbi{}](./c/dbi.md)
- [usr{}](./c/usr.md)
- [rsp{}](./c/rsp.md)
- [_autoload()](./fn/_autoload())
- [_shutdown()](./fn/_shutdown())
- [exceptionHanler()](./fn/exceptionHandler())
- [_d()](./fn/_d())
- [_e()](./fn/_e())
- [_from()](./fn/_from())
- [ESWC_Error()](./fn/ESWC_Error())
