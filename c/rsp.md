# rsp{}
> Version: 0.1.0 (04/04/2020)
```php
class rsp{

	protected $req=null;
	protected $_code=200;
	protected $_reason=null;
	protected $_err=false;
	protected $_charset='utf-8';
	protected $_headers=[];
	protected $_data=[];

	public function __construct(req $req){}

	abstract public function prepare(){}
	abstract public function execute(){}
	abstract public function parse(){}
	abstract public function send(){}

	public function send_headers(){}
	public function success($code=null,$reason=null){}
	public function error($code,$reason=null){}

	public function offsetSet($key,$val){}
	public function offsetGet($key){}
	public function offsetExists($key){}
	public function offsetUnset($key){}

	public function __set($key,$va){}
	public function __get($key):mixed{}
	public function __isset($key):bool{}
	public function __unset($key){}

	public function __toString():string{}

}
```

## Методы и классы
- Virtual
	- `int` $code
	- `string` $doctype
	- `string` $type
	- `array` $data
- Private
- Protected
- Public
