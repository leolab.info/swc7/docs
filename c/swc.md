# class swc{}
> Version: 0.1.0 (04/04/2020)
```php
class swc{

	private static $_vars=[];
	private static $_instance=null;

	public static function getInstance():\swc{}

	private function __clone(){}
	private function __wakeup(){}
	private function __construct(){}

	protected function __init(){}

	public static function run(){}

	public function start(){}
	public function prepare(){}
	public function parse(){}
	public function execute(){}
	public function render(){}
	public function send(){}
	public function finish(){}

	private function __getRsp():\swc\rsp{}

	public function __set($key,$val){}
	public function __unset($key){}
	public function __isset($key){}
	public function __get($key){}

}
```

Главный класс ядра системы.

## Методы и свойства
- Virtual
	- [$swc->req](#$req)
	- [$swc->cfg](#$cfg)
	- [$swc->usr](#$usr)
	- [$swc->rsp](#$rsp)
	- [$swc->dbi](#$dbi)
- Private
	- [swc::$_vars](#$_vars)
	- [swc::$_instance](#$_instance)
	- [$swc->_getRsp()](#_getRsp())
	- [$swc->__construct()](#__construct())
	- [$swc->__clone()](#__clone())
	- [$swc->__wakeup()](#__wakeup())
- Protected
	- [$swc->__init()](#__init())
- Publics
	- [swc::run()](#run())
	- [swc::getInstance()](#getInstance())
	- [$swc->start()](#start())
	- [$swc->prepare()](#prepare())
	- [$swc->parse()](#parse())
	- [$swc->execute()](#execute())
	- [$swc->render()](#render())
	- [$swc->send()](#send())
	- [$swc->finish()](#finish())
	- [$swc->__set()](#__set())
	- [$swc->__unset()](#__unset())
	- [$swc->__isset()](#__isset())
	- [$swc->__get()](#__get())

### $req
> Version: 0.1.0
```php
virtual \swc\req $req=null;
```
Экземпляр класса [\swc\req{}](./c/swc/req.md)

### $cfg
> Version: 0.1.0
```php
virtual array $cfg;
```
Конфигурация ядра системы.

### $usr
> Version: 0.1.0
```php
virtual \swc\usr $usr=null;
```
Экземпляр класса [\swc\usr{}](./c/swc/usr.md)

### $rsp
> Version: 0.1.0
```php
virtual \swc\rsp $rsp=null;
```
Экземпляр класса ответа:
- [\swc\rsp{}](./c/swc/rsp.md)
	- [\swc\act{}](./c/swc/act.md)
	- [\swc\get{}](./c/swc/get.md)
	- [\swc\page{}](./c/swc/page.md)

### $dbi
> Version: 0.1.0
```php
virtual \swc\dbi $dbi=null;
```
Экземпляр класса [\swc\dbi{}](./c/swc/dbi.md), или `NULL`

### $_vars
> Version: 0.1.0
```php
private static array $_vars=[];
```
Внутреннее хранилище виртуальных свойств

### $_instance
> Version: 0.1.0
```php
private static \swc $_instance=null;
```
Хранилище экземпляра класса

### getInstance()
> Version: 0.1.0
```php
public static function getInstance():\swc{}
```

Получить экземпляр класса.

### __clone()
> Version: 0.1.0
```php
private function __clone(){}
```
Заглушка

### __wakeup()
> Version: 0.1.0
```php
private function __wakeup(){}
```
Заглушка

### __construct()
> Version: 0.1.0
```php
private function __construct(){}
```
Заглушка

### __init()
> Version: 0.1.0
```php
protected function __init(){}
```
Метод инициализации класса.

Производит инициализацию свойств:
- [$swc->req](#$req)

### run()
> Version: 0.1.0
```php
public static function run(){}
```
Объединяющий метод

### start()
> Version: 0.1.0
```php
public static function start(){}
```
Запуск системы на выполнение.

Метод должен быть вызван сразу после получения экземпляра класса.

Метод производит инициализацию свойств:
- [$swc->cfg](#$cfg)
- [$swc->rsp](#$rsp)

### prepare()
> Version: 0.1.0
```php
public function prepare(){}
```
Подготовка к выполнению.

### parse()
> Version: 0.1.0
```php
public function parse(){}
```
Разобрать данные для обработки

### execute()
> Version: 0.1.0
```php
public function execute(){}
```
Выполнить действия

### render()
> Version: 0.1.0
```php
public function render(){}
```
Сформировать ответ

### send()
> Version: 0.1.0
```php
public function send(){}
```
Отправить данные

### finish()
> Version: 0.1.0
```php
public function finish(){}
```
Завершение работы

### __getRsp()
> Version: 0.1.0 (03/04/2020)
```php
private function __getRsp():\swc\rsp{}
```
Получить экземпляр ответа.

### __set()
> 0.1.0 (04/04/2020)
```php
public function __set($key,$val){}
```
Установить виртуальное свойство.

#### Exceptions
- [\swc\ESWC_PropertyReadOnly{}](./c/swc/ESWC_PropertyReadOnly.md)
- [\swc\ESWC_InvalidPropertyType{}](./c/swc/ESWC_InvalidPropertyType.md)
- [\swc\ESWC_UndefinedProperty{}](./c/swc/ESWC_UndefinedProperty.md)

### __unset()
> 0.1.0 (04/04/2020)
```php
public function __unset($key){}
```

Удалить виртуальное свойство

### __isset()
> 0.1.0 (04/04/2020)
```php
public function __isset($key):bool{}
```
Установлено ли виртуальное свойство

### __get()
> 0.1.0 (04/04/2020)
```php
public function __get($key):mixed{}
```
Получить значение виртуального свойства
