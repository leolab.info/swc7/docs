# \swc\ESWC_Error{}
> Version: 0.1.0 (04/04/2020)
```php
namespace swc;
class ESWC_Error extends Exception{}
```

Базовый класс исключений ядра системы.