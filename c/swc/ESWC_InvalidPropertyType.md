# ESWC_InvalidPropertyType
> Version: 0.1.0 (04/04/2020)
```php
namespace swc;
class ESWC_InvalidPropertyType extends ESWC_Error{
	public function __construct(class $o, mixed $val,string $type,string $name){}
}
```

## Использование
```php

class foo{

	private $_var=''; // string

	public function __set($key,$val){
		switch($key){
			case 'var':
				if(!is_string($val)){throw new \swc\ESWC_InvalidPropertyType($this,$val,'string',$key);}
				$this->$_var=$val;
		}
	}

}
```
## Методы и свойства
### __construct()
> Version: 0.1.0 (04/04/2020)
```php
public function __construct(class $o,mixed $val,string $type, string $name){}
```
##### $obj
`class` - класс для которого вызывается исключение.

##### $val
`mixed` - полученное значение свойства.

##### $type
`string` - ожидаемый тип значения свойства.

##### $name
`string` - имя свойства
