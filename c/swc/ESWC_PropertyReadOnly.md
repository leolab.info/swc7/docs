# \swc\ESWC_PropertyReadOnly
> 0.1.0 (04/04/2020)
```php
namespace swc;
class ESWC_PropertyReadOnly extends ESWC_Error{
	public function __construct(string|object $c, string $p){}
}
```

Наследуется от базового класса [\swc\ESWC_Error{}](./ESWC_Error.md)

Исключение, вызывается при попытке изменить значение свойства только для чтения.

## Методы и свойства
- [$::__construct()](#__construct())

### __conststruct()

#### Параметры
##### $c
`string|object`

Имя или экземпляр класса.
##### $p
`string`

Имя свойства