# ESWC_UndefinedProperty{}
> Version: 0.1.0 (04/04/2020)
```php
namespace swc;
class ESWC_UndefinedProperty extends ESWC_Error{
	public function __construct(object $obj, string $name){}
}
```

Неопределенное свойство.

## 
### __construct()
> Version: 0.1.0 (04/04/2020)
```php
public function __construct(object $obj, string $name){}
```

##### $obj
`object` - класс для которого вызывается исключение

##### $name
`string` - имя свойства.

