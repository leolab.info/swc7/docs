# fs::save()
> Version: 0.1.0 (04/04/2020)
```php
static public function save(string $fName, string $data):?{}
```
## Параметры
### $fName
`string` - имя файла для записи
### $data
`string` - данные для записи в файл
## Результат
## Исключения
- [\swc\ESWC_EmptyFileName()](../../c/swc/ESWC_EmptyFileName.md)
- [\swc\ESWC_FileCreateError()](../../c/swc/ESWC_FileCreateError.md)

## Изменения
### 2020-04-04
- Исключения вместо встроенных ошибок.
### 2019-03-10
### 2016-03-17