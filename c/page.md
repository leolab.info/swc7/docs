# page{}
> Version: 0.1.0 (04/04/2020)
```php
class page extends get{

	public function prepare(){}
	public function parse(){}
	public function execute(){}
	public function render(){}
	public function 

	private function _exists(string $path, string $file):bool{}
}
```
- [get{}](./get.md)

## Методы и свойства
- Virtual
- Public
	- [$page->prepare()](./page.class/prepare.md)
	- [$page->parse()](./page.class/parse.md)
	- [$page->execute()](./page.class/execute.md)
	- [$page->render()](./page.class/render.md)
- Protected
- Private
	- [$page->_exists()](./page.class/_exists.md)

### _exists()
