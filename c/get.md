# get{}
> Version: 0.1.0 (04/04/2020)
```php
class get extends rsp{

	protected $_tpl=null;
	protected $_content=null;

	private $_scripts=[];
	private $_styles=[];

	public function prepare(){}
	public function execute(){}
	public function parse(){}
	public function send(){}
	
	private function findHandler(string $h,?string $m=null,?string $s=null){}

	public function scriptAdd(string $name, ?string $content=null){}
	public function scriptDel(string $name){}

	public function get_tpl():string{}
	public function set_tpl(string $val){}
	public function isset_tpl(){}

	public function get_content(){}
	public function isset_content():bool{}
	
	public function get_scripts():array{}
	public function set_scripts(array $val){}
	public function isset_scripts(){}

	public function get_styles():array{}
	public function set_styles(array $val){}
	public function isset_styles():bool{}

	public function get_size():int{}
	public function isset_size():bool{}
	public function __toString():string{}

}
```
[rsp](./rsp.md)

## Методы и свойства
- Virtual
- Public
- Protected
- Private
